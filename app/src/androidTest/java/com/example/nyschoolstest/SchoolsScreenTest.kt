package com.example.nyschoolstest

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.nyschoolstest.models.NYCSchool
import com.example.nyschoolstest.models.SchoolsScreenState
import com.example.nyschoolstest.screens.SchoolsScreen
import com.example.nyschoolstest.screens.SchoolsScreenViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@RunWith(AndroidJUnit4::class)
class SchoolsScreenTest {

    @get: Rule
    val composeTestRule = createComposeRule()

    @Mock
    private lateinit var mockViewModel: SchoolsScreenViewModel

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun testMultipleSchoolCards() {
        val dummySchools = listOf(
            NYCSchool(
                databaseId = "someId",
                name = "Some Awesome School",
                overviewParagraph = "Visit us!",
                location = "It's in NY City!"
            ),
            NYCSchool(
                databaseId = "someId2",
                name = "Some Awesome School 2",
                overviewParagraph = "Visit us 2!",
                location = "It's in NY City 2!"
            )
        )
        Mockito.`when`(mockViewModel.getAllSchools()).thenAnswer {
            Mockito.`when`(mockViewModel.schoolsState).thenReturn(
                MutableStateFlow(
                    SchoolsScreenState.Success(
                        dummySchools
                    )
                )
            )
        }

        composeTestRule.setContent {
            SchoolsScreen(viewModel = mockViewModel)
        }

        composeTestRule.onNodeWithText("someId").assertIsDisplayed()
        composeTestRule.onNodeWithText("someId2").assertIsDisplayed()
    }
}