package com.example.nyschoolstest.repositories

import com.example.nyschoolstest.models.NYCSchool
import com.example.nyschoolstest.network.NYCSchoolsApi
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SchoolsRepositoryImplTest {

    @Mock
    private lateinit var mockApi: NYCSchoolsApi

    @InjectMocks
    private lateinit var targetRepo: SchoolsRepositoryImpl

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun `when getting all Schools, api returns valid list of schools`() = runBlocking {
        // Given
        Mockito.`when`(mockApi.getAllSchools()).thenReturn(listOf(mock(NYCSchool::class.java)))

        // When
        val result = targetRepo.getAllSchools()

        // Then
        TestCase.assertTrue(result.isNotEmpty())
    }
}