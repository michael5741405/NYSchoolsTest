package com.example.nyschoolstest.screens

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.nyschoolstest.models.SchoolsScreenState
import com.example.nyschoolstest.repositories.SchoolsRepositoryContract
import junit.framework.TestCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toCollection
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@OptIn(ExperimentalCoroutinesApi::class)
class SchoolsScreenViewModelTest {

    @JvmField
    @Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var mockRepo: SchoolsRepositoryContract

    @InjectMocks
    private lateinit var targetViewModel: SchoolsScreenViewModel

    @Before
    fun setup() {
        Dispatchers.setMain(UnconfinedTestDispatcher())
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun `when repo returns empty list, emit empty success`() = runBlocking {
        // Given
        Mockito.`when`(mockRepo.getAllSchools()).thenReturn(emptyList())
        val statesRecorder = mutableListOf<SchoolsScreenState>()

        // When
        targetViewModel.getAllSchools()
        targetViewModel.schoolsState.toCollection(statesRecorder)

        // Then
        val firstResult = statesRecorder.firstOrNull()
        val secondResult = statesRecorder[1]
        TestCase.assertTrue(firstResult is SchoolsScreenState.Loading)
        TestCase.assertTrue(secondResult is SchoolsScreenState.EmptySuccess)
    }
}