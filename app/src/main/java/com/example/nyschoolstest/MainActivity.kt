package com.example.nyschoolstest

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.nyschoolstest.screens.SchoolsScreen
import com.example.nyschoolstest.screens.SchoolsScreenViewModel

class MainActivity : AppCompatActivity() {

    private val schoolsViewModel: SchoolsScreenViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SchoolsScreen(schoolsViewModel)
        }
    }
}