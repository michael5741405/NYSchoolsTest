package com.example.nyschoolstest.screens

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nyschoolstest.models.SchoolsScreenState
import com.example.nyschoolstest.repositories.SchoolsRepositoryContract
import com.example.nyschoolstest.repositories.SchoolsRepositoryImpl
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

//TODO("Setup a viewModel Factory to properly set the injection of dependencies.")
class SchoolsScreenViewModel(
    private val schoolsRepository: SchoolsRepositoryContract = SchoolsRepositoryImpl()
) : ViewModel() {

    val schoolsState: StateFlow<SchoolsScreenState> by lazy { _schoolsState }
    private val _schoolsState: MutableStateFlow<SchoolsScreenState> =
        MutableStateFlow(SchoolsScreenState.None)

    fun getAllSchools() {
        _schoolsState.value = SchoolsScreenState.Loading
        viewModelScope.launch {
            try {
                val response = schoolsRepository.getAllSchools()
                if (response.isEmpty()) {
                    _schoolsState.emit(SchoolsScreenState.EmptySuccess)
                } else {
                    _schoolsState.emit(SchoolsScreenState.Success(response))
                }
            } catch (e: Exception) {
                _schoolsState.emit(SchoolsScreenState.Failure(e.localizedMessage.toString()))
            }

        }
    }
}