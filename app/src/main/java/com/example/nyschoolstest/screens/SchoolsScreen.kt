package com.example.nyschoolstest.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.nyschoolstest.models.NYCSchool
import com.example.nyschoolstest.models.SchoolsScreenState

@Composable
fun SchoolsScreen(viewModel: SchoolsScreenViewModel) {

    val schoolsState = viewModel.schoolsState

    LaunchedEffect(Unit) {
        viewModel.getAllSchools()
    }

    when (val state = schoolsState.collectAsState().value) {
        SchoolsScreenState.EmptySuccess -> {
            // TODO()
        }

        is SchoolsScreenState.Failure -> {
            // TODO()
        }

        SchoolsScreenState.Loading -> {
            // TODO()
        }

        SchoolsScreenState.None -> {
            // TODO()
        }

        is SchoolsScreenState.Success -> DisplayAllSchools(state.schools)
    }

}

@Composable
fun DisplayAllSchools(schools: List<NYCSchool>) {
    LazyColumn(
        modifier = Modifier.background(Color.White)
    ) {
        items(schools) { school ->
            SchoolCard(school)
        }
    }
}

@Composable
fun SchoolCard(school: NYCSchool) {
    Column(
        modifier = Modifier
            .padding(4.dp)
            .background(Color.Gray)
            .padding(4.dp)
    ) {
        val textModifier = Modifier.padding(vertical = 4.dp)
        Text(
            text = "School id: ${school.databaseId}",
            color = Color.Black,
            modifier = textModifier
        )
        Text(
            text = "School name: ${school.name}",
            color = Color.Black,
            modifier = textModifier
        )
        Text(
            text = "School location: ${school.location}",
            color = Color.Black,
            modifier = textModifier
        )
    }
}

@Preview
@Composable
fun PreviewSchoolCard() {
    val school = NYCSchool(
        databaseId = "someId",
        name = "Some Awesome School",
        overviewParagraph = "Visit us!",
        location = "It's in NY City!"
    )
    SchoolCard(school)
}