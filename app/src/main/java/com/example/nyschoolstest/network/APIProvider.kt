package com.example.nyschoolstest.network

import com.example.nyschoolstest.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

// TODO("JAN 31st, Setup propper Dependency Injection")
object ApiProvider {

    private const val BASE_URL = "https://data.cityofnewyork.us/resource/"

    private val loggingInterceptor = HttpLoggingInterceptor().apply {
        level = if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }
    }

    // TODO("JAN 31st, Add logging interceptor or auth interceptors if required later on")
    private val okHttpClient: OkHttpClient = OkHttpClient
        .Builder()
        .addInterceptor(loggingInterceptor)
        .build()

    private val retrofit = Retrofit
        .Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val nycSchoolsApi: NYCSchoolsApi = retrofit.create(NYCSchoolsApi::class.java)
}