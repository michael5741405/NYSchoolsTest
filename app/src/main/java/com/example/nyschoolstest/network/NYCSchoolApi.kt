package com.example.nyschoolstest.network

import com.example.nyschoolstest.models.NYCSchool
import retrofit2.http.GET

interface NYCSchoolsApi {
    @GET("s3k6-pzi2.json")
    suspend fun getAllSchools(): List<NYCSchool>
}