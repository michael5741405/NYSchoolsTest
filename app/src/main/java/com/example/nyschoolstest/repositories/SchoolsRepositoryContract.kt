package com.example.nyschoolstest.repositories

import com.example.nyschoolstest.models.NYCSchool
import com.example.nyschoolstest.network.ApiProvider
import com.example.nyschoolstest.network.NYCSchoolsApi
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface SchoolsRepositoryContract {
    suspend fun getAllSchools(): List<NYCSchool>
}

class SchoolsRepositoryImpl(
    private val api: NYCSchoolsApi = ApiProvider.nycSchoolsApi,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
): SchoolsRepositoryContract {

    // TODO("Jan 31st: Add a real local source")
    private var availableSchools: List<NYCSchool> = emptyList()

    override suspend fun getAllSchools(): List<NYCSchool> {
        try {
            return withContext(dispatcher) {
                val newSchools = api.getAllSchools()
                availableSchools = newSchools
                return@withContext newSchools
            }
        } catch (e: Exception) {
            // TODO("JAN 31st: Log Exceptions")
            throw e
        }
    }
}